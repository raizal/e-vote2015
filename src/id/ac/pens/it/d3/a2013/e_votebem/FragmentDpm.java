package id.ac.pens.it.d3.a2013.e_votebem;

import id.ac.pens.it.d3.a2013.e_votebem.db.Candidate;
import id.ac.pens.it.d3.a2013.e_votebem.db.DBAdapter;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Constant;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Request;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Request.Event;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.meetme.android.horizontallistview.HorizontalListView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class FragmentDpm extends Fragment {
	private ArrayList<Candidate> candidates;

	private HorizontalListView list;
	
	private Button skip,logout;
	private View.OnClickListener buttonEvent = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(v==skip){
				
			}else{
				((MainActivity) getActivity()).switchToLogin();
			}
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.main_page, container, false);
		skip = (Button) v.findViewById(R.id.skip);
		skip.setVisibility(View.GONE);
		logout = (Button) v.findViewById(R.id.logout);
		skip.setOnClickListener(buttonEvent);
		logout.setOnClickListener(buttonEvent);
		candidates = DBAdapter.getInstance(getActivity()).getDPM(MainActivity.idgroup);			
		list = (HorizontalListView) v.findViewById(R.id.list);
		list.setAdapter(new Adapter());
		return v;
	}

	private class Adapter extends BaseAdapter {

		@Override
		public int getCount() {
			return candidates != null ? candidates.size() : 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Candidate c = candidates != null ? candidates.get(position) : null;
			convertView = getActivity().getLayoutInflater().inflate(R.layout.kandidat_item, parent, false);
			if (c != null) {
				ImageView image = (ImageView) convertView.findViewById(R.id.kand);
				// image.setImageBitmap(Utils.getFile(c.getFoto()));
				final ProgressBar loading = (ProgressBar) convertView.findViewById(R.id.loading);
				ImageLoader.getInstance().displayImage(Constant.BASE + c.getFoto(), image,new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {
						loading.setVisibility(View.VISIBLE);
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
						
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						loading.setVisibility(View.GONE);
					}
					
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						// TODO Auto-generated method stub
						
					}
				});
				// ImageLoader.getInstance().displayImage("http://www.androidhive.info/wp-content/uploads/2014/07/2.jpg",
				// image);

				image.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle("Konfirmasi").setCancelable(false).setMessage("Apakah Anda yakin dengan pilihan Anda?").setPositiveButton("Yakin", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								HashMap<String, String> data = new HashMap<String, String>();
								data.put(Constant.REQUEST, Constant.REQUEST_VOTE);
								data.put(Constant.KEY, id.ac.pens.it.d3.a2013.e_votebem.helper.Config.getToken(getActivity()));
								data.put(Constant.USER, MainActivity.nrp);
								data.put(Constant.USER_TOKEN, MainActivity.token);
								data.put(Constant.POLL, Constant.POLL_DPM);
								data.put(Constant.CANDIDATE, c.getId() + "");
								new Request(Constant.AUTH, data, new Event() {

									@Override
									public void onFinish(String result) {
										JSONObject data;
										String text = "Terima kasih telah melakukan voting...";
										try {
											data = new JSONObject(result);
											if (data.getString("request").equals("accepted")) {

											}
										} catch (JSONException e) {

										}
										Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
										((MainActivity) getActivity()).switchToLogin();
									}
								});
							}
						}).setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						}).create();
						dialog.show();

					}
				});
			}
			return convertView;
		}

	}
}

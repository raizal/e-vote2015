package id.ac.pens.it.d3.a2013.e_votebem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class FragmentToken extends Fragment implements View.OnClickListener {

	private EditText nrp, token;

	public FragmentToken() {
	}

	public void reset() {
		if (nrp != null && token != null) {
			nrp.setText("");
			token.setText("");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_token, container, false);
		nrp = (EditText) v.findViewById(R.id.nrp);
		token = (EditText) v.findViewById(R.id.token);

		v.findViewById(R.id.button).setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {
		MainActivity.nrp = nrp.getText().toString();
		MainActivity.token = token.getText().toString();
		((MainActivity) getActivity()).signInBEM();
	}
}

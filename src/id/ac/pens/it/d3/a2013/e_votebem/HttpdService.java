package id.ac.pens.it.d3.a2013.e_votebem;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import fi.iki.elonen.NanoHTTPD;

public class HttpdService extends Service {

	private final static int PORT = 8080;

	private static MyHTTPD httpd;

	public HttpdService() {
		if (httpd == null) {
			try {
				httpd = new MyHTTPD();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			if (!httpd.isAlive())
				httpd.start();
		} catch (Exception e) {
			if (httpd == null) {
				try {
					httpd = new MyHTTPD();
					httpd.start();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		}
		return Service.START_STICKY;
	}

	@Override
	public void onDestroy() {
		if (httpd != null && httpd.isAlive()) {
			httpd.stop();
		}
		super.onDestroy();
	}

	private class MyHTTPD extends NanoHTTPD {
		public MyHTTPD() throws IOException {
			super(PORT);
		}

		@Override
		public Response serve(IHTTPSession session) {			
			String uri = session.getUri();
			String msg = "";
								
			if(session.getParms().containsKey("command") && session.getParms().get("command").equals("reset")){
				MainActivity.reinit();
				msg = "{status:0}";				
				return newFixedLengthResponse(Response.Status.OK, "application/json", msg);
			}else if(uri.equals("/")||uri.equals("")){
				
				HashMap<String, String> data = new HashMap<String, String>();
				data.put("status", MainActivity.status+"");
				data.put("active_client", MainActivity.nrp);				
				msg = new JSONObject(data).toString();
				return newFixedLengthResponse(Response.Status.OK, "application/json", msg);
			}
			
			return newFixedLengthResponse(msg);
		}

	}
}

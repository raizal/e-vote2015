package id.ac.pens.it.d3.a2013.e_votebem;

import id.ac.pens.it.d3.a2013.e_votebem.db.DBAdapter;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Constant;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Request;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Request.Event;
import id.ac.pens.it.d3.a2013.e_votebem.helper.Config;
import id.ac.pens.it.d3.a2013.e_votebem.helper.TokenDialog;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class MainActivity extends ActionBarActivity {

	private static MainActivity instance;

	public static int idgroup = -1;
	public static String nrp, token, nama;
	public static String bem = "yes", dpm = "yes";

	private Handler handler = new Handler();
	
	public static int status = 0;	

	private Runnable currentPostDelayed = new Runnable() {
		
		@Override
		public void run() {
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(MainActivity.this, "Waktu Anda telah habis...", Toast.LENGTH_SHORT).show();
					if(activepage==1){
						switchToDPM();
					}else if(activepage==2){
						switchToLogin();
					}
				}
			}); 			
		}
	};
	
	private int activepage = -1;

	// 0 token, 1 bem, 2 dpm

	public static void reinit() {
		idgroup = -1;
		nrp = "";
		token = "";
		nama = "";
		bem = "yes";
		dpm = "yes";
		instance.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(instance, "Device telah direset oleh operator...", Toast.LENGTH_LONG).show();
				instance.handler.removeCallbacks(instance.currentPostDelayed);
			}
		});
		instance.switchToLogin();
	}

	public void switchToLogin() {
		runOnUiThread(new Runnable() {
			public void run() {
				status = 0;
				handler.removeCallbacks(currentPostDelayed);
				if (activepage != 0) {
					FragmentToken ft = new FragmentToken();
					activepage = 0;
					getSupportFragmentManager().beginTransaction().setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
					.replace(R.id.fragmentParentViewGroup,ft, "token").commit();
					ft.reset();
				}

			}
		});
	}

	public void switchToBEM() {
		status = 1;
		activepage = 1;
		MainActivity.this.getSupportFragmentManager().beginTransaction().setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
		.replace(R.id.fragmentParentViewGroup, new FragmentBem(), "bem").commit();		
		handler.postDelayed(currentPostDelayed, Config.getTime(this)*1000);		

	}

	public void switchToDPM() {
		status = 1;
		activepage = 2;
		MainActivity.this.getSupportFragmentManager().beginTransaction().setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
		.replace(R.id.fragmentParentViewGroup, new FragmentDpm(), "dpm").commit();
		handler.postDelayed(currentPostDelayed, Config.getTime(this)*1000);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(this);
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.writeDebugLogs(); // Remove for release app
		
		ImageLoader.getInstance().init(config.build());
		
		instance = this;
		setContentView(R.layout.activity_main);
		startService(new Intent(this, HttpdService.class));
		if (savedInstanceState == null) {
			switchToLogin();
		}
		init();
	}

	private void init() {

		Map<String, String> data = new HashMap<String, String>();
		data.put(Constant.REQUEST, Constant.REQUEST_LIST);
		data.put(Constant.KEY, Config.getToken(this));

		new Request(Constant.AUTH, data, new Event() {

			@Override
			public void onFinish(String result) {
				try {
					JSONObject res = new JSONObject(result);
					if (res.getString("request").equals("denied")) {
						TokenDialog.showDialog(MainActivity.this);
					}
				} catch (JSONException e) {
					TokenDialog.showDialog(MainActivity.this);
					e.printStackTrace();
				}

			}

		});

	}

	public void signInBEM() {
		signIn(nrp, token,Constant.POLL_BEM);
	}

	public void signInDPM() {
		signIn(nrp, token,Constant.POLL_DPM);
	}
	
	public void signIn(final String nrp, final String token,String polType) {

		Map<String, String> data = new HashMap<String, String>();
		data.put(Constant.REQUEST, Constant.VOTE);

		data.put(Constant.KEY, Config.getToken(this));
		data.put(Constant.USER_TOKEN, token);
		data.put(Constant.USER, nrp);
		data.put(Constant.POLL, polType);
		
		try {
			new Request(Constant.AUTH, data, new Event() {

				@Override
				public void onFinish(String result) {
					try {
						JSONObject data = new JSONObject(result);

						if (!data.getString("request").equals("denied")) {							
							MainActivity.idgroup = Integer.parseInt(data.getString("id_part"));
							MainActivity.nama = data.getString("responder_name");
							MainActivity.bem = data.getString("poll_presbem");
							MainActivity.dpm = data.getString("poll_dpm");
							if (MainActivity.bem.equals("yes"))
								switchToBEM();
							else if (MainActivity.dpm.equals("yes"))
								switchToDPM();
							else if (!MainActivity.bem.equals("done") && !MainActivity.dpm.equals("done")) {
								Toast.makeText(MainActivity.this, "Maaf, Anda sudah melakukan voting!", Toast.LENGTH_LONG).show();
								MainActivity.reinit();
							}else{
								Toast.makeText(MainActivity.this, "Maaf, status pemilih Anda tidak aktif!\nSilahkan hubungi operator terdekat!", Toast.LENGTH_LONG).show();
								MainActivity.reinit();
							}
						} else {
							Toast.makeText(MainActivity.this, "Pastikan data Anda benar...", Toast.LENGTH_SHORT).show();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	protected void onDestroy() {
		stopService(new Intent(this, HttpdService.class));
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (getSupportFragmentManager().getBackStackEntryCount() > 0)
			super.onBackPressed();
	}
}

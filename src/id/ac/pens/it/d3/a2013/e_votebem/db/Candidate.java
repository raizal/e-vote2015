package id.ac.pens.it.d3.a2013.e_votebem.db;

public class Candidate {

	public Candidate(int id, String nama, String visi, int group, String foto, String type, String groupName) {
		super();
		this.id = id;
		this.nama = nama;
		this.visi = visi;
		this.group = group;
		this.type = type;
		this.groupName = groupName;
		this.foto = foto;
	}

	public Candidate(int id, String nama, String visi, int group, String foto) {
		super();
		this.id = id;
		this.nama = nama;
		this.visi = visi;
		this.group = group;
		this.foto = foto;
	}

	private String nama, visi, foto;

	private int id, group;

	private String type, groupName;

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getVisi() {
		return visi;
	}

	public void setVisi(String visi) {
		this.visi = visi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

}

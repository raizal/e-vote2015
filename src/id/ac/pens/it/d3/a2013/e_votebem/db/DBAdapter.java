package id.ac.pens.it.d3.a2013.e_votebem.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter extends SQLiteOpenHelper {

	private static DBAdapter instance;

	public static DBAdapter getInstance(Context context) {
		if (instance == null)
			instance = new DBAdapter(context);
		return instance;
	}

	public DBAdapter(Context context) {
		super(context, "evote", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {

			db.execSQL("CREATE TABLE \"kelompok\"( \"id\"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"nama\"  TEXT, \"type\" TEXT);");
			db.execSQL("CREATE TABLE \"candidate\" ( \"id\"  INTEGER PRIMARY KEY NOT NULL, \"id_group\"  INTEGER, \"nama\"  TEXT, \"visi\"  TEXT, \"photo_uri\"  TEXT);");

		} catch (Exception e) {
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
	}

	public void purge(){
		SQLiteDatabase db = getWritableDatabase();

		try {
			db.execSQL("delete from kelompok");
			db.execSQL("delete from candidate");
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			db.close();
		}
	}
	
	public void insertGroup(int id, String name, String type) {
		SQLiteDatabase db = getWritableDatabase();

		try {

			ContentValues data = new ContentValues();
			data.put("id", id);
			data.put("nama", name);
			data.put("type", type);

			db.insert("kelompok", null, data);
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			db.close();
		}
	}

	public void insertCandidate(Candidate c) {
		SQLiteDatabase db = getWritableDatabase();

		try {

			ContentValues data = new ContentValues();

			data.put("id", c.getId());
			data.put("id_group", c.getGroup());
			data.put("nama", c.getNama());
			data.put("Visi", c.getVisi());
			data.put("photo_uri", c.getFoto());

			db.insert("candidate", null, data);
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			db.close();
		}
	}

	public ArrayList<Candidate> getBEM(){
		SQLiteDatabase db = getWritableDatabase();
		ArrayList<Candidate> data =new ArrayList<Candidate>();
		Cursor c = null;
		try {
			c = db.rawQuery("select a.* from candidate a, kelompok b where a.id_group=b.id and b.type=\"bem\"", null);			
			if(c.moveToFirst()){
				do{
					data.add(new Candidate(c.getInt(0), c.getString(2), c.getString(3), c.getInt(1), c.getString(4), "bem","BEM"));					
				}while(c.moveToNext());
			}
		} catch (Exception e) {

		} finally {
			if(c!=null)
				c.close();
			db.close();			
		}
		return data;
	}
	
	public int getGroupId(String name){
		SQLiteDatabase db = getWritableDatabase();
		Cursor c = null;
		int id = -1;
		try {
			c = db.rawQuery("select id from kelompok where nama=\""+name+"\"", null);			
			if(c.moveToFirst()){
				id = c.getInt(0);
			}
		} catch (Exception e) {

		} finally {
			if(c!=null)
				c.close();
			db.close();			
		}
		return id;
	}
	
	public ArrayList<Candidate> getDPM(int groupid){
		SQLiteDatabase db = getWritableDatabase();
		ArrayList<Candidate> data =new ArrayList<Candidate>();
		Cursor c = null;
		try {
			c = db.rawQuery("select * from candidate where id_group=\""+groupid+"\"", null);			
			if(c.moveToFirst()){
				do{
					data.add(new Candidate(c.getInt(0), c.getString(2), c.getString(3), groupid, c.getString(4), "dpm",""));					
				}while(c.moveToNext());
			}
		} catch (Exception e) {

		} finally {
			if(c!=null)
				c.close();
			db.close();			
		}
		return data;
	}
	
}

package id.ac.pens.it.d3.a2013.e_votebem.etc;

public class Constant {

	public static final String TOKEN = "key_app";
	public static final String KEY = "token";
	public static final String REQUEST = "request";
	public static final String VOTE = "vote";
	public static final String USER = "user";
	public static final String USER_TOKEN = "user_token";

	public static final String POLL = "poll";
	public static final String POLL_BEM = "presbem";
	public static final String POLL_DPM = "dpm";

	public static final String CANDIDATE = "candidate";

	public static final String BASE = "http://192.168.1.100";
	public static final String AUTH = BASE + "/voter/auth";

	public static final String REQUEST_LIST = "list", REQUEST_VOTE = "vote";
	
}
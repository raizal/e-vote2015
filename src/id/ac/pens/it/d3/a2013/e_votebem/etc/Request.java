package id.ac.pens.it.d3.a2013.e_votebem.etc;

import java.util.Map;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;

public class Request {

	public interface Event {
		public void onFinish(String result);
	}

	public Request(final String url, final Map<String, String> data, final Event event) {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				String body = "{request:\"denied\"}";
				try {
					HttpRequest req = HttpRequest.post(url).connectTimeout(5000);
					if (data != null)
						req.form(data);

					body = req.body();
				} catch (Exception e) {
				}

				return body;
			}

			protected void onPostExecute(String result) {
				if (event != null)
					event.onFinish(result);
			}
		}.execute();
	}
}

package id.ac.pens.it.d3.a2013.e_votebem.helper;

import id.ac.pens.it.d3.a2013.e_votebem.etc.Constant;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Config {
	
	private static SharedPreferences preference;
	
	public static boolean isTokenAvailable(Context context){			
		return getToken(context)!=null;		
	}
	
	public static void setToken(String token,Context context){
		if(preference==null){
			preference = PreferenceManager.getDefaultSharedPreferences(context);
		}
				
		preference.edit().putString(Constant.TOKEN, token).commit();		
	}
	
	public static String getToken(Context context){
		if(preference==null){
			preference = PreferenceManager.getDefaultSharedPreferences(context);
		}
		
		return preference.getString(Constant.TOKEN, "1234567789");
	}
	
	public static int getTime(Context context){
		if(preference==null){
			preference = PreferenceManager.getDefaultSharedPreferences(context);
		}
		
		return preference.getInt("time", 120);
	}
	
	public static void setTime(int time,Context context){
		if(preference==null){
			preference = PreferenceManager.getDefaultSharedPreferences(context);
		}
		
		preference.edit().putInt("time", time).commit();
	}
	
}
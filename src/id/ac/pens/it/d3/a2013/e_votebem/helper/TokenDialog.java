package id.ac.pens.it.d3.a2013.e_votebem.helper;

import id.ac.pens.it.d3.a2013.e_votebem.MainActivity;
import id.ac.pens.it.d3.a2013.e_votebem.db.Candidate;
import id.ac.pens.it.d3.a2013.e_votebem.db.DBAdapter;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Constant;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Request;
import id.ac.pens.it.d3.a2013.e_votebem.etc.Request.Event;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.widget.EditText;
import android.widget.Toast;

public class TokenDialog {

	private static AlertDialog dialog;

	private static ProgressDialog waitingDialog;

	public static void showDialog(final android.app.Activity context) {
		if (dialog == null) {
			final EditText input = new EditText(context);
			dialog = new AlertDialog.Builder(context).setTitle("Error!!").setCancelable(false).setMessage("Masukkan Api-Key terlebih dahulu!").setView(input).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					String value = input.getText().toString();
					Config.setToken(value, context);
					TokenDialog.this.dialog.dismiss();
					requestData(context, value);
				}
			}).create();
		}
		dialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				MainActivity.status = 0;				
			}
		});
		MainActivity.status = 1;
		dialog.show();
	}

	public static void requestData(final Activity context, String key) {
		Map<String, String> data = new HashMap<String, String>();
		data.put(Constant.REQUEST, Constant.REQUEST_LIST);
		data.put(Constant.KEY, key);
		showLoadingDialog(context);
		new Request(Constant.AUTH, data, new Event() {

			@Override
			public void onFinish(String result) {
				Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
				dismissLoadingDialog(context);
				try {
					JSONObject data = new JSONObject(result);
					if (data.getString("request").equals("denied")) {
						Toast.makeText(context, "Api-Key Expired...\nSilahkan masukkan Api-Key yang baru...", Toast.LENGTH_SHORT).show();
						showDialog(context);
					} else {
						// TODO insert data
						DBAdapter.getInstance(context).purge();
						int time = data.getInt("time");
						Config.setTime(time, context);
						JSONArray groups = data.getJSONArray("data");
						for (int i = 0; i < groups.length(); i++) {
							JSONObject o = groups.getJSONObject(i);

							if (o.getString("poll").equals("pres")) {
								DBAdapter.getInstance(context).insertGroup(0, "bem", "bem");
								JSONArray candidates = o.getJSONArray("candidate");
								for (int j = 0; j < candidates.length(); j++) {
									JSONObject c = candidates.getJSONObject(j);
									Candidate candidate = new Candidate(c.getInt("id"), c.getString("name"), c.getString("vision"), 0, c.getString("photo"));
									DBAdapter.getInstance(context).insertCandidate(candidate);
//									if (!Utils.isExist(candidate.getFoto()))
//										Utils.downloadFile(candidate.getFoto(), new Utils.Event() {
//
//											@Override
//											public void before() {
//												context.runOnUiThread(new Runnable() {
//													public void run() {
//														Toast.makeText(context, "DOWNLOAD IMAGE", Toast.LENGTH_SHORT).show();
//													}
//												});
//											}
//
//											@Override
//											public void after() {
//												context.runOnUiThread(new Runnable() {
//													public void run() {
//														Toast.makeText(context, "BERHASIL DOWNLOAD IMAGE", Toast.LENGTH_SHORT).show();
//													}
//												});
//
//											}
//										});
								}
							} else if (o.getString("poll").equals("dpm")) {
								int idGroup = Integer.parseInt(o.getString("id_part"));
								DBAdapter.getInstance(context).insertGroup(idGroup, o.getString("id_name"), "dpm");
								JSONArray candidates = o.getJSONArray("candidate");
								for (int j = 0; j < candidates.length(); j++) {
									JSONObject c = candidates.getJSONObject(j);
									Candidate candidate = new Candidate(c.getInt("id"), c.getString("name"), "", idGroup, c.getString("photo"));
									DBAdapter.getInstance(context).insertCandidate(candidate);
//									if (!Utils.isExist(candidate.getFoto()))
//										Utils.downloadFile(candidate.getFoto(), new Utils.Event() {
//
//											@Override
//											public void before() {
//												context.runOnUiThread(new Runnable() {
//													public void run() {
//														Toast.makeText(context, "DOWNLOAD IMAGE", Toast.LENGTH_SHORT).show();
//													}
//												});
//											}
//
//											@Override
//											public void after() {
//												context.runOnUiThread(new Runnable() {
//													public void run() {
//														Toast.makeText(context, "BERHASIL DOWNLOAD IMAGE", Toast.LENGTH_SHORT).show();
//													}
//												});
//
//											}
//										});
								}
							}

						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	private static void showLoadingDialog(Context context) {
		if (waitingDialog == null) {
			waitingDialog = new ProgressDialog(context);
			waitingDialog.setTitle("Mohon tunggu sebentar...");
			waitingDialog.setCancelable(false);
			waitingDialog.setMessage("Sedang memproses permintaan...");
		}
		waitingDialog.show();
	}

	private static void dismissLoadingDialog(Context context) {
		if (waitingDialog != null) {
			waitingDialog.dismiss();
		}
	}
}

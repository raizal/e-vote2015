package id.ac.pens.it.d3.a2013.e_votebem.helper;

import id.ac.pens.it.d3.a2013.e_votebem.etc.Constant;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;

public class Utils {

	private static final String PATH = "/evote";

	public interface Event {
		public void before();

		public void after();
	};

	public static Bitmap getFile(String file) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeFile(PATH + file, options);
		return bitmap;
	}

	public static boolean isExist(String file) {
		return new File(Environment.getExternalStorageDirectory().getAbsolutePath() + PATH + file).isFile();
	}

	public static void downloadFile(String url, Event event) {
		downloadFile(Constant.BASE + url, Environment.getExternalStorageDirectory().getAbsolutePath() + PATH + url, event);
	}

	public static void downloadFile(final String url, final String dest_file_path, final Event event) {
		new Thread(new Runnable() {
			public void run() {
				if (event != null)
					event.before();
				new File(Environment.getExternalStorageDirectory().getAbsolutePath() + PATH +"/candidate/").mkdirs();

				toFile(getBitmapFromURL(url), dest_file_path);
				if (event != null)
					event.after();
			}
		}).start();
	}

	public static Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			// Log exception
			e.printStackTrace();
			return null;
		}
	}

	public static void toFile(Bitmap bitmap, String file) {
		// create a file to write bitmap data
		File f = new File(file);
		try {
			f.createNewFile();

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, 0 /* ignored for PNG */, bos);
			byte[] bitmapdata = bos.toByteArray();

			// write the bytes in file
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(bitmapdata);
			fos.flush();
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
